import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:solid_software_test_task/color_cubit.dart';

class Home extends StatelessWidget {
  final MaterialColor materialColor;
  const Home({Key? key, required this.materialColor}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    void changeColor() {
      BlocProvider.of<ColorCubit>(context).nextColor();
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Flutter Solid Software Test Task'),
      ),
      body: GestureDetector(
        onTap: changeColor,
        child: Container(
          color: materialColor,
          child: Center(
            child: Text(
              'Hello there',
              style: TextStyle(
                  fontSize: 24.0,
                  color: materialColor.shade50,
                  backgroundColor: materialColor
              ),
            ),
          ),
        ),
      ),
    );
  }
}
