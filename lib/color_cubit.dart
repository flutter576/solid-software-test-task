import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:solid_software_test_task/material_color_random.dart';

class ColorCubit extends Cubit<MaterialColor> {
  ColorCubit() : super(MaterialColorRandom.first());

  final _colorRandom = MaterialColorRandom();

  void nextColor() {
    emit(
      _colorRandom.next()
    );
  }
}
