import 'dart:math';
import 'package:flutter/material.dart';

class MaterialColorRandom {
  static const List<MaterialColor> _colors = Colors.primaries;
  static const int _firstColorIndex = 5;

  static MaterialColor first() => _colors[_firstColorIndex];

  final _random = Random();
  int _previousIndex = _firstColorIndex;

  MaterialColor next() {
    final index = chooseRandomIndexDifferentFromPrevious();
    _previousIndex = index;
    return _colors[index];
  }

  int chooseRandomIndexDifferentFromPrevious() {
    int randomIndex;
    do {
      randomIndex = _random.nextInt(_colors.length);
    } while (randomIndex == _previousIndex);

    return randomIndex;
  }
}
